package com.livejournal.voylaf.forecasts.domain.users

sealed trait Ban {}

case object NotBanned extends Ban

case object Infinitely extends Ban

case class Until(date: java.util.Date) extends Ban
