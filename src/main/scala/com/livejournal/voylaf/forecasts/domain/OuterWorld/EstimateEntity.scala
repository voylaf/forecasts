package com.livejournal.voylaf.forecasts.domain.OuterWorld

import java.util.UUID

trait EstimateEntity {
  def uuid: UUID

  def name: String

  def showName: String
}

case class EE[F[_]](
             uuid: UUID,
             name: String,
             showName: String,
             )

//case class Club(
//                 uuid: UUID,
//                 name: String,
//                 showName: String,
//               )
//
//case class Sportsman(
//                      uuid: UUID,
//                      name: String,
//                      showName: String
//                    )
