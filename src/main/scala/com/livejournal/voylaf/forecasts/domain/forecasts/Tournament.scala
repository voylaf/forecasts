package com.livejournal.voylaf.forecasts.domain.forecasts

import com.livejournal.voylaf.forecasts.domain.OuterWorld.EResults
import com.livejournal.voylaf.forecasts.domain.parsing.RawData

import scala.util.Try

case class Tournament[F[_], Sum, R](
                                     current: Standings[F, Sum],
                                     closedTime: java.util.Date,
                                     rules: Rules[F, Sum, R],
                                     getResult: RawData => EResults[F, R],
                                     forecasts: F[Forecast],
                                   )

////todo: Algebra + validation
//object Tournament {
//  def step[F[_], Sum, R](rawData: RawData)(t: Tournament[F, Sum, R]): Tournament[F, Sum, R]  = {
//    for {
//      result <- Try(t.getResult).toOption
//      check <- t.rules.check(t.forecasts, result)
//    }
//  }
//}