package com.livejournal.voylaf.forecasts.domain.users

import java.util.UUID

trait UserRepositoryActions[F[_]] {
  def create(user: User): F[User]

  def update(user: User): F[User]

  def get(userId: UUID): F[User]

  def delete(userId: UUID): F[User]

  def findByUsername(username: String): F[User]

  def deleteByUsername(username: String): F[User]

  def page(pageSize: Int, offset: Int): F[List[User]]
}
