package com.livejournal.voylaf.forecasts.domain

sealed trait DomainValidation {
  def errorMessage: String
}

case object UserNotFound extends DomainValidation {
  def errorMessage = "User not found"
}

case class UserAlreadyExists(username: String) extends DomainValidation {
  def errorMessage: String = s"User $username already exists"
}

case class UserAuthenticationFail(username: String) extends DomainValidation {
  override def errorMessage: String = s"Failed authentication"
}

case object UsernameHasSpecialCharacters extends DomainValidation {
  def errorMessage: String = "Username cannot contain special characters."
}

case object PasswordDoesNotMeetCriteria extends DomainValidation {
  def errorMessage: String = "Password must be at least 10 characters long, including an uppercase and a lowercase letter, one number and one special character."
}