package com.livejournal.voylaf.forecasts.domain.forecasts

import com.livejournal.voylaf.forecasts.domain.users.User

case class Standings[F[_], A](
                 current: F[(User, A)]
                 )
