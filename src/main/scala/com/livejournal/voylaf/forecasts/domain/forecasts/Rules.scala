package com.livejournal.voylaf.forecasts.domain.forecasts

import cats.Functor
import cats.syntax.functor._
import com.livejournal.voylaf.forecasts.domain.OuterWorld.{EResults, EstimateEntity}

case class Rules[F[_] : Functor, Sum, R](
                                          check: (F[Forecast], EResults[F, R]) => Standings[F, Sum],
                                          aggregate: (Standings[F, Sum], Standings[F, Sum]) => Standings[F, Sum],
                                          limit: Points,
                                          eeCosts: F[(EstimateEntity, Points)],
                                        ) {
  def ees: F[EstimateEntity] = eeCosts.map(_._1)
}
