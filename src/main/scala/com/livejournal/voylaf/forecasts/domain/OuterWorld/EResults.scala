package com.livejournal.voylaf.forecasts.domain.OuterWorld

case class EResults[F[_], R](res: F[(EstimateEntity, R)])
