package com.livejournal.voylaf.forecasts.domain.users

import java.util.UUID

case class User(
                 userId: UUID,
                 username: String,
                 showName: String,
                 //todo: authentication via jwt
                 //todo: right classes instead Strings
                 passwordHash: String,
                 email: String,
                 bannedUntil: Ban
               //todo: roles
               )
